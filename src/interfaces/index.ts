export interface CurrencyDictionary {
  [key: string]: Currency;
}

export interface Currency {
  name: string;
  selected: boolean;
  highlighted: boolean;
  expanded: boolean;
  fixedSavings?: BaseLockedProduct[];
  lockedStaking?: BaseLockedProduct[];
}

export interface BEProductMap {
  [key: string]: {
    fixedSavings?: {
      days: string;
      APY: string;
    }[];
    lockedStaking?: {
      days: string;
      APY: string;
    }[];
  };
}

interface BaseLockedProduct {
  APY: string;
  days: string;
}
