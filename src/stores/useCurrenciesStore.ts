import { BEProductMap, Currency, CurrencyDictionary } from "@/interfaces";
import { defineStore } from "pinia";
import { useStorage } from "@vueuse/core";
import _ from "lodash";

export const useCurrenciesStore = defineStore("currenciesStore", {
  state: () => ({
    availableCurrencies: [
      "ADA",
      "BUSD",
      "USDT",
      "USDC",
      "MATIC",
      "CAKE",
      "SOL",
      "BNB",
    ],
    currencyDictionary: useStorage(
      "currencyDictionary",
      {} as CurrencyDictionary
    ),
  }),
  getters: {
    selectedCurrencies: (state) => {
      return _.pickBy(
        state.currencyDictionary,
        (currency) => currency.selected
      );
    },
  },
  actions: {
    async refreshProducts() {
      const res = await fetch("/api/products", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          currencies: this.availableCurrencies,
        }),
      });
      const productMap: BEProductMap = await res.json();
      Object.entries(productMap).forEach(([currencyName, products]) => {
        this.currencyDictionary[currencyName].fixedSavings =
          products.fixedSavings;
        this.currencyDictionary[currencyName].lockedStaking =
          products.lockedStaking;
      });
    },
    async setupCurrencies() {
      this.availableCurrencies.forEach((currency) => {
        if (this.currencyDictionary[currency]) return;

        this.currencyDictionary[currency] = {
          name: currency,
          selected: false,
          highlighted: false,
          expanded: true,
        };
      });
    },
    retrieveMatchingCurrencies(query: string): Currency[] {
      return _.filter(_.values(this.currencyDictionary), (currency) =>
        currency.name.includes(query.toUpperCase())
      );
    },
    toggleSelectedCurrency(currency: Currency) {
      this.currencyDictionary[currency.name].selected =
        !this.currencyDictionary[currency.name].selected;
    },
    highlightCurrencies(currencies: Currency[]) {
      currencies.forEach((currency) => (currency.highlighted = true));
      setTimeout(() => {
        currencies.forEach((currency) => (currency.highlighted = false));
      }, 2000);
    },
  },
});
