const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      "^/api": {
        target: "http://express-api:3000",
        changeOrigin: true,
        logLevel: "debug",
        secure: false,
        pathRewrite: { "^/api": "/" },
      },
    },
  },
});
